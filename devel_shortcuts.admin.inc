<?php

/**
 * @file
 * This module holds the administratve pages for the Devel Shortcuts module.
 */


/**
 * Administrative settings to enable or disable the tabs.
 */
function devel_shortcuts_admin_settings() {

  $form['devel_shortcuts_edit_entity_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a tab on the entity to quickly access editing the entity type.'),
    '#default_value' => variable_get('devel_shortcuts_edit_entity_type', TRUE),
    '#description' => t('Display a tab above all entities that when clicked, takes the user to the editing page for that entity.'),
  );
  $form['devel_shortcuts_view_entities_for_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a tab on the entity to quickly access views that list entities of this type.'),
    '#default_value' => variable_get('devel_shortcuts_view_entities_for_type', TRUE),
    '#description' => t('Display a tab above all entities that when clicked, takes the user to a view listing entities.'),
  );
  $form['devel_shortcuts_view_entities_for_type_edit_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a tab on the entity edit page to quickly access views that list entities of this type.'),
    '#default_value' => variable_get('devel_shortcuts_view_entities_for_type_edit_page', TRUE),
    '#description' => t('Display a tab on the content type editing page which takes the user to a view listing entities of that type.'),
  );
  // TODO add setting here to allow for the view URL to use hyphens or underscores

  return system_settings_form($form);
}
