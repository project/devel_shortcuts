<?php

/**
 * @file
 * This module holds the actual functionality for functions
 * for Development Shortcuts
 */

/**
 * Menu callback; goes to the link to edit the current content type.
 */
function devel_shortcuts_edit_entity_type($type, $object, $name = NULL) {
  switch ($type) {
    case 'node':
      $node_type = $object->type;
      drupal_goto('admin/structure/types/manage/' . $node_type . '/fields');
      break;

    case 'user':
      drupal_goto('admin/config/people/accounts/fields');
      break;

    case 'taxonomy_term':
      $vocabulary_machine_name = $object->vocabulary_machine_name;
      drupal_goto('admin/structure/taxonomy/' . $vocabulary_machine_name . '/fields');
      break;

  }
}


function devel_shortcuts_view_entities_for_type($type, $object, $name = NULL) {
  switch ($type) {
    case 'node':
      $node_type = $object->type;
      $path_plural = devel_shortcuts_pluralize($node_type);  // The path you want to check.
      $item_plural = menu_get_item($path_plural);
      if ($item_plural && $item_plural['access']) {
        drupal_goto($path_plural);  // Path exists and the user has access to it.
      }elseif($item = menu_get_item($node_type) && $item['access']){
        drupal_set_message("consider pluralizing the path to this view");
        drupal_goto($node_type);
      }
      else {
        // Path does not exist or the user doesn't have access to it.
        drupal_set_message(l("Consider making a view", 'admin/structure/views/add') , " to see all nodes of type " . $node_type);
        drupal_goto('admin/content', array('type' => $node_type, 'promote' => 'All', 'status' => 'All', 'sticky' => 'All') );
      }
      break;
    case 'user':
      drupal_goto('admin/people');
      break;

    case 'taxonomy_term':
      /// TODO figure out a better path for this
      $vocabulary_machine_name = $object->vocabulary_machine_name;
      drupal_goto('admin/structure/taxonomy/' . $vocabulary_machine_name . '/fields');
      break;
  }
}


function devel_shortcuts_view_entities_for_type_edit_page($node_type) {

      $path_plural = devel_shortcuts_pluralize($node_type);  // The path you want to check.
      $item_plural = menu_get_item($path_plural);
      if ($item_plural && $item_plural['access']) {
        drupal_goto($path_plural);  // Path exists and the user has access to it.
      }elseif($item = menu_get_item($node_type) && $item['access']){
        ds("consider pluralizing the path to this view");
        drupal_goto($node_type);
      }
      else {
        // Path does not exist or the user doesn't have access to it.
        ds(l("Consider making a view", 'admin/structure/views/add') , " to see all nodes of type " . $node_type);
        drupal_goto('admin/content', array('type' => $node_type, 'promote' => 'All', 'status' => 'All', 'sticky' => 'All') );
      }

}

function devel_shortcuts_pluralize($node_type){

 if($node_type[-1] == 's')
    return $node_type .'es';
  else
    return $node_type . 's';

}
